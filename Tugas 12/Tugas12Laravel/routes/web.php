<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'utama']);
Route::get('/register', [AuthController::class, 'regist']);

Route::post('/kirim', [AuthController::class, 'send']);

Route::get('/master', function(){
    return view('layouts.master');
});

Route::get('/table', function(){
    return view('halaman.table');
});

Route::get('/data-table', function(){
    return view('halaman.datatable');
});

//Membuat CRUD Cast

//create data:route untuk mengarah ke form tambah cast
Route::get('/cast/create', [CastController::class, 'create']);
//Route untuk menyimpan data inputan ke table cast
Route::post('/cast', [CastController::class, 'store']);

//read data:route untuk menampilkan semua data di table cast
Route::get('/cast', [CastController::class, 'index']);

//route untuk detail data berdasarkan id
Route::get('/cast/{id}', [CastController::class, 'show']);

//update data:route untuk mengarah ke form edit cast
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);

//route untuk update data berdasarkan di table cast
Route::put('/cast/{id}', [CastController::class, 'update']);

//delete data:route untuk menghapus data berdasarkan id
Route::delete('/cast/{id}', [CastController::class, 'destroy']);