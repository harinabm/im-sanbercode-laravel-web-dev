@extends('layouts.master')

@section('title')
Halaman Dashboard
@endsection

@section('sub-title')
Dashboard
@endsection

@section('content')
    <h2>Selamat Datang {{$namaDepan}} {{$namaBelakang}}</h2>
    <h3>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h3>
@endsection