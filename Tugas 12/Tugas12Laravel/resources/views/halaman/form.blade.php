@extends('layouts.master')

@section('title')
Sign Up Form
@endsection

@section('sub-title')
Buat Account Baru!
@endsection

@section('content')
    <form action="/kirim" method="post">
        @csrf
        <label>First Name:</label><br>
        <input type="text" name="fname"><br><br>
        <label>Last Name:</label><br>
        <input type="text" name="lname"><br><br>
        <label>Gender:</label><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>
        <label>Nationality:</label><br>
        <select name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="japanese">Japanese</option>
            <option value="chinese">Chinese</option>
            <option value="american">American</option>
            <option value="british">British</option>
        </select><br><br>
        <label>Language Spoken:</label><br>
        <input type="checkbox" name="language">Bahasa Indonesia <br>
        <input type="checkbox" name="language">English <br>
        <input type="checkbox" name="language">Other <br><br>
        <label >Bio:</label><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection