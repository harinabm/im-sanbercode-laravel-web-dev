<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Berlatih Function PHP</h1>
    <?php

        echo "<h3> Soal No 1 Greetings </h3>";

        function greetings($nama){
            echo "Halo $nama, Selamat Datang di Sanbercode! <br>";
        }

        greetings("Bagas");
        greetings("Wahyu");
        greetings("Harina Budi Mulyani");

        echo "<br>";

        echo "<h3>Soal No 2 Reverse String</h3>";
        
        function reverse($kataAwal){
            $panjangkata = strlen($kataAwal);
            $tampung = "";
            for ($i=($panjangkata - 1); $i>=0; $i--){
                $tampung .= $kataAwal[$i];
            }
            return $tampung;
        }

        function reverseString($kataAkhir){
            $string=reverse($kataAkhir);
            echo $string . "<br>";
        }

        reverseString("nama peserta");
        reverseString("Sanbercode");
        reverseString("We Are Sanbers Developers");
        echo "<br>";

        echo "<h3>Soal No 3 Palindrome </h3>";

        function palindrome($kata){
            $balik = reverse($kata);
            if ($kata === $balik){
                echo "$kata => True <br>";
            }else{
                echo "$kata => false <br>";
            }
        }

        palindrome("civic"); // true
        palindrome("nababan"); // true
        palindrome("jambaban"); // false
        palindrome("racecar"); // true


        echo "<h3>Soal No 4 Tentukan Nilai </h3>";
        
        function tentukan_nilai($angka){
            if($angka >= 85 && $angka < 100){
                return "$angka => Sangat Baik <br>";
            }else if($angka >= 70 && $angka < 85){
                return "$angka => Baik <br>";
            }else if($angka >= 60 && $angka < 70){
                return "$angka => Cukup <br>";
            }else return "$angka => Kurang <br>";
        }
        
        echo tentukan_nilai(98); //Sangat Baik
        echo tentukan_nilai(76); //Baik
        echo tentukan_nilai(67); //Cukup
        echo tentukan_nilai(43); //Kurang

    ?>
</body>
</html>